#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "bst.h"

static char *toUppercase(char *s) {
    /*OK*/
    char *u = malloc(sizeof(char)*(strlen(s)+1));
    strcpy(u, s);
    int i = 0;
    while(u[i] != 0) {
        u[i] = toupper(u[i]);
        i++;
    }
    return u;
}


static BinNode *BST_node_new(char *k) {
    /*OK*/
    BinNode *b = malloc(sizeof(BinNode));
	b->key = k;
	b->left = NULL;
	b->right = NULL;
	b->cont = 1;
	return b;
}

BST * BST_new() {
    /*OK*/
    BST *b = malloc(sizeof(BST));
    b->root = NULL;
	return b;
}

BinNode* BST_find_node(BinNode* node, char* key,int* finded){
    if(!node) return NULL;
    if(!node->left && !node->right) return node;
    int ret = strcmp(key,node->key);
    if(ret < 0){
      //printf("SINISTRO %s\n",key);
      if(node->left == NULL) return node;
      else return BST_find_node(node->left,key,finded);
    }
    else if(ret == 0){ 
      //printf("!%s!\n",node->key);
      *finded = 1;
      return node;
    }
    else if(ret > 0){
      //printf("DESTRO %s\n",key);
      if(node->right == NULL) return node;
      else return BST_find_node(node->right,key,finded);
    }
  return NULL;
}

BinNode *BST_insert(BST *b, char *k) {
        BinNode* root = b->root;
        int* finded = (int*)malloc(sizeof(int));
        *finded = 0;
        char* ku = toUppercase(k);
        //printf("INSERISCO %s\n",ku);
        BinNode* node = BST_find_node(root,ku,finded);
        if(!node){
            node = BST_node_new(ku);
            b->root = node;
         }
        if(*finded == 0){
          int ret = strcmp(ku,node->key);
          if(ret < 0){
            node->left = BST_node_new(ku);
            node = node->left;
          }
          else{
            node->right = BST_node_new(ku);
            node = node->right;
          }
        }
        else node->cont++;
        return node; // c
}

int BST_find(BST * b, char *k) {
        BinNode* root = b->root;
        int* finded = (int*)malloc(sizeof(int));
        *finded = 0;
        char* ku = toUppercase(k);
        BinNode* node = BST_find_node(root,ku,finded);
        //printf("Trovato: %s\n",node->key);
        if(*finded) return node->cont; 
        else return 0;
}

static void BST_print_aux(BinNode * t, int level, char pos) {
    /* OK, MODIFICABILE */
	if (t == NULL)
		return;

    int i;
    for (i = 0; i < level - 1; i++) {
    	printf("   ");
    }

    if (level > 0) {
    	printf(" %c:--", pos);
    }

    printf("key: |%s|\toccorrenze: %d\n", t->key, t->cont);

    BST_print_aux(t->left, level + 1, 'l');
    BST_print_aux(t->right, level + 1, 'r');
}

void BST_print(BST * b){
    /* OK, MODIFICABILE */
    if(b != NULL) return BST_print_aux(b->root, 0, ' ');
}

BinNode* _mostFrequentString(BinNode* node){
    if(node == NULL) return BST_node_new("");
    BinNode* left = NULL;
    BinNode* right = NULL;
    if(!node->left || !node->right){
      if(!node->left && !node->right) return node;
      if(!node->left){
        right = _mostFrequentString(node->right);
        left = BST_node_new("");
      }
      if(!node->right){
        left = _mostFrequentString(node->left);
        right = BST_node_new("");
      }
    }
    left = _mostFrequentString(node->left);
    right = _mostFrequentString(node->right);
    
    BinNode* aux;
    BinNode* temp;
    aux = left->cont > right->cont ? left : right;
    temp = aux->cont > node->cont ? aux : node;
    //printf("|%s|",node->key);
    return temp;
}

char *mostFrequentString(BST *t){
        BinNode* root = t->root;
        BinNode* ret = _mostFrequentString(root); // per consentire compilazione
        return ret->key;
}

int _isBalanced(BinNode* node,int flag){
    //if(!node) printf("DASDSAD\n");
    if(!node->left || !node->right){
      //printf("VISITO NODO: |%s|\n",node->key);
      if(!node->left && !node->right) return 1;
      if(flag) return 0;
      if(!node->left){
        //printf("DESTRA\n");
         _isBalanced(node->right,1);
       }
      if(!node->right){
        //printf("SINISTRA\n");
         _isBalanced(node->left,1);
       }
    }
    else{
      //printf("---------------\n");
      return _isBalanced(node->right,flag) && _isBalanced(node->left,flag);
    }
    return 0;
}

int isBalanced(BST *t) {
        BinNode* root = t->root;
        return _isBalanced(root,0); // per consentire compilazione
}



