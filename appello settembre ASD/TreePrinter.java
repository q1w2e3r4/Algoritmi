 public class TreePrinter{
    private Node root;
    
    public TreePrinter(Node r){
        root = r;
    }
    
    private static void printTree(Node n, int depth) {

        if (n == null)
            return;
        for (int i = 0; i < depth; i++)
            System.out.print("- ");
        if (n.getIsDir())
            System.out.println(n.getNome() + "/");
        else
            System.out.println(n.getNome() + " (" + n.getSize() +"B)");
        if (n.getIsDir())    
            printTree(n.getFirstChild(), depth + 1);
        printTree(n.getNextSibling(), depth);
    }

    public void printTree(){
        printTree(root,0);
    }

   private static void printTreeSize(Node n, int depth) {

        if (n == null)
            return;

        for (int i = 0; i < depth; i++)
            System.out.print("- ");
        if (n.getIsDir())
            System.out.println(n.getNome() + "/ (" + n.getSize() + "B)");
        else
            System.out.println(n.getNome() + " (" + n.getSize() + "B)");
        if (n.getIsDir())    
            printTreeSize(n.getFirstChild(), depth + 1);
        printTreeSize(n.getNextSibling(), depth);
    }

    public void printTreeSize(){
        printTreeSize(root,0);
    }

   private static void printTreeComplete(String s, Node n, String currentPath) {
        if (n == null) return;
        if (s.compareTo("*") == 0 || s.compareTo(n.getNome()) == 0){
            if (n.getIsDir())
                System.out.println(currentPath + n.getNome() + "/");
            else
                System.out.println(currentPath + n.getNome());
        }
        if (n.getIsDir())
            printTreeComplete(s, n.getFirstChild(), currentPath + n.getNome() + "/");
        printTreeComplete(s, n.getNextSibling(), currentPath);
   }

    public void printTreeFullPath() {
        printTreeComplete("*", root, "");
    }
    
    private void printSize(Node n, String currentPath){
        if (n == null)
            return;
        int size=n.getSize();
        String cancellabile = "cancellabile";
        Node figlio = n.getFirstChild();
        while (figlio !=null){
            if (!figlio.getIsDir())
                size += figlio.getSize();
            else
                cancellabile = "NON cancellabile";
            figlio = figlio.getNextSibling();
        }
        System.out.println(currentPath + n.getNome() + "/ (" + size + "B) " + cancellabile);
        figlio = n.getFirstChild();
        while (figlio !=null){
            if (figlio.getIsDir())
                printSize(figlio,currentPath + n.getNome() + "/");
            figlio = figlio.getNextSibling();
        }
    }

    public void printSize(){
        printSize(root, "");
    }
 
}
