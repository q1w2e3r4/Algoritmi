#include <stdlib.h>
#include <stdio.h>

#include "queue.h"
#include "max_level.h"

int max_level(node_tree * n) {

	if (n == NULL)
		return -1;

	queue * q = new_queue();
	queue * next_q = new_queue();
	node_tree * node = n;
	
	int depth = 0;
	int max_level = 0;
	int max_sum = 0;

	int sum = 0;

	while(node != NULL) {

		//printf("Visiting node: %d\n", get_node_info(node));
		sum += get_node_info(node);

		if (get_first_child(node) != NULL)
			queue_enqueue(next_q, get_first_child(node));

		if (get_next_sibling(node) != NULL)
			node = get_next_sibling(node);
		else
			node = NULL;

		if (node == NULL && queue_size(q) > 0)
			node = queue_dequeue(q);

		if (node == NULL) {

			printf("Visited all nodes at level %d - sum: %d\n", depth, sum);
			if (sum > max_sum) {
				max_level = depth;
				max_sum = sum;
			}

			depth += 1;

			sum = 0;

			queue * temp = q;
			q = next_q;
			next_q = temp;

			node = queue_dequeue(q);
		}
	}

	delete_queue(q);
	delete_queue(next_q);

	return max_level;
}