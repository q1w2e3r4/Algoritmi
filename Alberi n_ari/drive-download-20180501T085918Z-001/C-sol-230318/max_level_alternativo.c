#include <stdlib.h>
#include <stdio.h>

#include "queue.h"
#include "max_level.h"
#include "tree.h"


int max_level_aus(node_tree* n,int* sommaChiaviFratelli,int* maxAttuale){	
	//Visita in postOrdine					
	if(get_first_child(n) != NULL){	//se ha figli li visito
		*sommaChiaviFratelli = 0;	
		max_level_aus(get_first_child(n),sommaChiaviFratelli,maxAttuale);	
	}
	if(get_next_sibling(n) != NULL){	//se ha fratelli li visito, facendo la sommatoria della chiave del nodo dove sono + tutti i suoi fratelli che visiterò
		(*sommaChiaviFratelli) += get_node_info(n) + max_level_aus(get_next_sibling(n),sommaChiaviFratelli,maxAttuale);	
	}else{								//se il nodo non ha fratelli faccio ritornare la sua chiave
		return get_node_info(n);
	}
	if((*sommaChiaviFratelli) > (*maxAttuale)){			//confronto il massimo, se è maggiore lo aggiorno
		printf("sommaChiaviFratelli %d > maxAttuale %d aggiorno maxAttuale\n",*sommaChiaviFratelli,*maxAttuale);
		(*maxAttuale) = (*sommaChiaviFratelli);
	}
	(*sommaChiaviFratelli) = 0;							//chiudo un livello e azzero la somma dei fratelli
	

	return *maxAttuale;
}


int max_level(node_tree * n) {
	int maxAttuale = 0;			//variabile per maxTemporaneo
	int max = 0;				//variabile per maxVincente
	max_level_aus(n,&max,&maxAttuale);
	return maxAttuale;
}