#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "parse_tree.h"

char * next_line(char * buffer, int size) {
	size_t s = size;
	ssize_t res = getline(&buffer, &s, stdin);
	if (res >= 0)
		return buffer;
	else
		return NULL;
}

int parse_int(char * buffer) {
	return atoi(buffer);
}

int get_depth_line(char * buffer) {
	char lvlchar[1] = "-";
	return strspn(buffer, lvlchar);
}

int parse_tree_ric(node_tree * current, int current_depth, char * buffer, int size) {
	int depth = get_depth_line(buffer);
	do {	
		// case depth < current_depth
		if (depth < current_depth)
			return depth;
		
		int value = parse_int(buffer + depth);
		node_tree * node = new_node_tree(value);

		// case depth == current_depth
		if (depth == current_depth) 
			set_next_sibling(current, node);

		// case depth > current_depth
		else
			set_first_child(current, node);

		char * b = next_line(buffer, size);
		if (b != NULL)
			depth = parse_tree_ric(node, depth, buffer, size);
		else
			depth = -1; // EOF

	} while(depth > 0);

	return 0;
}

node_tree * parse_tree() {
	int size = 128;
	char * buffer = malloc(size);
	char * b = next_line(buffer, size);

	node_tree * root = NULL;

	if (b != NULL) {
		int depth = get_depth_line(buffer);
		if (depth != 0)
			return root;
		int value = parse_int(buffer + depth);
		root = new_node_tree(value);
		b = next_line(buffer, size);
		if (b != NULL)
			parse_tree_ric(root, 0, buffer, size);
	}
	free(buffer);
	return root;
}
