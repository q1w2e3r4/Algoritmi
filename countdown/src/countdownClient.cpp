#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <countdown/CountdownAction.h>

using namespace std;

int main (int argc, char **argv)
{
  ros::init(argc, argv, "client");
  
  
  //mi connetto al Server alla classe CountdownAction
  actionlib::SimpleActionClient<countdown::CountdownAction> countclient("countdownServer", true);

  ROS_INFO("Attendo il server");
  //attendo la preparazione e settaggio del server
  countclient.waitForServer();

  ROS_INFO("Connesso");
  //setto il messaggio
  countdown::CountdownGoal goal;
  goal.number = 10;
  countclient.sendGoal(goal);


  //imposto una durata,se il server non termina nel suo stato prima del timer termino il clien con la return
  int flag = countclient.waitForResult(ros::Duration(10));
  if (flag){
    ROS_INFO("Azione completata");
  }else{
    ROS_INFO("Tempo esaurito");
  }
    
  return 0;
}
