#include <stdlib.h>
#include <stdio.h>

#include "queue.h"
#include "max_level.h"

int max_level(node_tree * n) {
	queue* coda=new_queue();
	node* nodo=n;
	queue_enqueue(coda,nodo);
	node* fasullo=NULL;
	queue_enqueue(coda,fasullo);
	int max=0;
	int current_max=0;
	while(queue_size(coda)!=0){
		node* extr=queue_dequeue(coda);
		if(extr==NULL){ //ho trovato il nodo fasullo
			if(current_max>max) max=current_max; 
			if(queue_size(coda)==0){//la coda è vuolta
				return max;
			}else{
				queue_enqueue(coda,fasullo);
				extr=queue_dequeue(coda);
				current_max=0;
			}
		}
	
		current_max+=extr->value;
		extr=extr->first_child;
		while(extr){
			queue_enqueue(coda,extr);
			extr=extr->next_sibling;
		}
	}
	return max;
}
