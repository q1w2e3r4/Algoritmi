#include <stdlib.h>
#include <stdio.h>
#include "bst.h"

typedef struct _bst_node {
	int key;
    void * value;
    struct _bst_node * left;
    struct _bst_node * right;
} _bst_node;

typedef struct _bst {
	_bst_node * root;
} _bst;

bst * bst_new(int k, void * v) {
	_bst* new=malloc(sizeof(_bst));
	new->root=malloc(sizeof(_bst_node));
	new->root->key=k;
	new->root->value=v;
	new->root->left=NULL;
	new->root->right=NULL;
	return (bst*)new;
}

void aux_insert(_bst_node* node, int k, void* v){
	if(k < node->key && node->left==NULL){
		node->left=malloc(sizeof(_bst_node));
		node->left->key=k;
		node->left->value=v;
		node->left->left=NULL;
		node->left->right=NULL;
		return;
	}else if(k > node->key && node->right==NULL){
		node->right=malloc(sizeof(_bst_node));
		node->right->key=k;
		node->right->value=v;
		node->right->left=NULL;
		node->right->right=NULL;
		return;
	}
	if(k < node->key){
		aux_insert(node->left,k,v);
	}else if(k > node->key){
		aux_insert(node->right,k,v);
	}else if(k == node->key){
		node->value=v;
		return;
	}
	return;
}

void bst_insert(bst * b, int k, void * v) {
	_bst* bb=(_bst*)b;
	aux_insert(bb->root,k,v);
}

_bst_node* aux_find(_bst_node* node, int k){
	if(node==NULL) return NULL;
	if(k < node->key){
		aux_find(node->left,k);
	}else if(k > node->key){
		aux_find(node->right,k);
	}else if(k == node->key){
		return node;
	}
	return;
}

void * bst_find(bst * b, int k) {
	_bst* bb=(_bst*)b;
	if(b==NULL || bb->root==NULL) return;
	_bst_node* nn=aux_find(bb->root,k);
	if(nn==NULL) return NULL;
	return nn->value;
}

_bst_node* aux_find_min(_bst_node* node){
	if(node->left==NULL) return node;
	aux_find_min(node->left);
}

int bst_find_min(bst * b) {
	_bst* bb=(_bst*)b;
	_bst_node* nn=aux_find_min(bb->root);
	return nn->key;
}

_bst_node* aux_remove_min(_bst_node* node){
	if(node->left==NULL){
		_bst_node* tr=node->right;
		free(node);
		return tr;
	}
	node->left=aux_remove_min(node->left);
	return node;
}

void bst_remove_min(bst * b) {
	_bst* bb=(_bst*)b;
	if(bb->root==NULL) return;
	bb->root=aux_remove_min(bb->root);
}

_bst_node* aux_find_remove(_bst_node* node,int k){
	if(node==NULL) return NULL;
	if(k < node->key){
		node->left=aux_find_remove(node->left,k);
	}else if(k > node->key){
		node->right=aux_find_remove(node->right,k);
	}else{//ho trovato il nodo da rimuovere
		if(node->right==NULL && node->left!=NULL){
			_bst_node* aux=node->left;
			free(node);
			return aux;
		}if(node->right!=NULL && node->left==NULL){
			_bst_node* aux=node->right;
			free(node);
			return aux;
		}if(node->right==NULL && node->left==NULL){
			free(node);
			return NULL;
		}
		//il nodo ha due figli
		_bst_node* rimuovi=node;
		node=aux_find_min(node->right);
		_bst_node* nn=malloc(sizeof(_bst_node));
		nn->key=node->key;
		nn->value=node->value;
		nn->right=aux_remove_min(rimuovi->right);
		nn->left=rimuovi->left;
		free(rimuovi);
		node=nn;

		
	}
	return node;
	
}


void bst_remove(bst * b, int k) {
	_bst* bb=(_bst*)b;
	if(bb->root==NULL || b==NULL) return;
	bb->root=aux_find_remove(bb->root,k);
}



static void bst_delete_aux(_bst_node * t) {

	if (t == NULL)
		return;

	bst_delete_aux(t->left);
	bst_delete_aux(t->right);
	free(t);
}

void bst_delete(bst * b) {

	if (b == NULL)
		return;

	_bst * t =(_bst*) b;
	bst_delete_aux(t->root);
	free(t);
}

void bst_print_aux(_bst_node * t, int level) {
    
	if (t == NULL)
		return;

    int i;
    for (i = 0; i < level - 1; i++) {
    	printf("   ");
    }

    if (level > 0) {
    	printf(" |--");
    }

    printf("%d\n", t->key);

    bst_print_aux(t->left, level + 1);
    bst_print_aux(t->right, level + 1);
}

void bst_print(bst * b){
	_bst * t =(_bst*) b;
    return bst_print_aux(t->root, 0);
}

_bst_node* bst_predecessor_aux(_bst_node* node, int k){
	_bst_node* t=NULL;
	while(node!=NULL){
		if(node->key < k){
			t=node;
			node=node->right;
		}else{
			node=node->left;
		}
	}
	return t;
}

int bst_predecessor(bst * b, int k) {
	_bst* bb=(_bst*)b;
	_bst_node* n;
	if(b==NULL || bb->root==NULL) return NULL;
	if(k <= bb->root->key){
		//n=aux_find(bb->root,k);
		n=bst_predecessor_aux(bb->root->left,k);
	}else if(k > bb->root->key){
		//n=aux_find(bb->root,k);
		n=bst_predecessor_aux(bb->root->right,k);
	}
	if(n==NULL) return NULL;
    return n->key;
}














