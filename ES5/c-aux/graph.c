#include "graph.h"
#include "linked_list.h"
#include <stdlib.h>
#include <stdio.h>

typedef enum {UNEXPLORED, EXPLORED, EXPLORING} STATUS;

typedef struct {
    void * value;
    linked_list * out_edges;
    // keep track status
    STATUS state;
    int timestamp;

} graph_node_struct;

typedef struct {
	GRAPH_TYPE type;
    linked_list * nodes;
} graph_struct;

graph * graph_new(GRAPH_TYPE type) {
  graph_struct* g = (graph_struct*)malloc(sizeof(graph_struct));
  g->type = type;
  g->nodes = linked_list_new();
  return g;
}

linked_list * graph_get_nodes(graph * gg) {
  graph_struct* g = (graph_struct*)gg;
  return g->nodes;
}

linked_list * graph_get_neighbors(graph * g, graph_node * n) {
    graph_node_struct* nn = (graph_node_struct*)n;
    return nn->out_edges;
}

graph_node * graph_add_node(graph * gg, void * value) {
	graph_struct* g = (graph_struct*)gg;
  graph_node_struct* node = (graph_node_struct*)malloc(sizeof(graph_node_struct));
  node->value = value;
  node->out_edges = linked_list_new();
  linked_list_add(g->nodes,(void*)node);
  return node;
}

void graph_add_edge(graph * gg, graph_node * from, graph_node * to) {
	graph_struct* g = (graph_struct*) gg;
  graph_node_struct* f = (graph_node_struct*) from;
  graph_node_struct* t = (graph_node_struct*) to;
  linked_list_add(f->out_edges,(void*)t);
  if(g->type == UNDIRECTED) linked_list_add(t->out_edges,(void*)f);
  return;
}

void * graph_get_node_value(graph * g, graph_node * nn) {
	graph_node_struct* n = (graph_node_struct*)nn;
  return n->value;
}

void graph_delete(graph * gg) {
	graph_struct * g = (graph_struct *) gg;
	linked_list_iterator * i = linked_list_iterator_new(g->nodes);
	while (i != NULL) {
		graph_node_struct * n = linked_list_iterator_getvalue(i);
		linked_list_delete(n->out_edges);
		free(n);
		i = linked_list_iterator_next(i);
  }
    linked_list_delete(g->nodes);
    free(g);
}

int get_path(graph_node* ss, graph_node* tt, linked_list* l){
  graph_node_struct* t = (graph_node_struct*)tt;
  graph_node_struct* s = (graph_node_struct*)ss;
  s->state = EXPLORED;
  linked_list_add(l,(void*)s->value);
  if(s->value == t->value) return 1;
  linked_list_iterator* out = linked_list_iterator_new(s->out_edges);
  while(out){
    graph_node_struct* node = (graph_node_struct*)linked_list_iterator_getvalue(out);
    if(node->state == UNEXPLORED){
      int ret = get_path(node,t,l);
      if(ret) return 1;
      else linked_list_remove_last(l);
    }
    out = linked_list_iterator_next(out);
  }
  return 0;
}

linked_list * graph_get_path(graph * gg, graph_node * s, graph_node * t) {
  graph_struct* g = (graph_struct*)gg;
  linked_list_iterator* i = linked_list_iterator_new(g->nodes);
  while(i){
    graph_node_struct* node = (graph_node_struct*) linked_list_iterator_getvalue(i);
    node->state = UNEXPLORED;
    i = linked_list_iterator_next(i);
  }
  
  linked_list* list = linked_list_new();
  
  int found = get_path(s,t,list);
  if(found) return list;
  linked_list_delete(list);
  return NULL;
}

int sweep_aux(graph_node_struct* u,int time, char* format_string){
  if(u->state != UNEXPLORED) return 0;
  u->state = EXPLORING;
  u->timestamp = time;
  int local = 1;
  linked_list_iterator* it = linked_list_iterator_new(u->out_edges);
  while(it){
    graph_node_struct* v = (graph_node_struct*)linked_list_iterator_getvalue(it);
    printf("<");
    printf(format_string,u->value);
    printf(",");
    printf(format_string,v->value);
    printf("> is a ");
    if(v->state == EXPLORED){
      if(u->timestamp < v->timestamp) printf("FORWARD\n");
      else printf("CROSS\n");
    }
    if(v->state == EXPLORING) printf("BACK\n");
    if(v->state == UNEXPLORED){
      printf("TREE\n");
      local += sweep_aux(v,time + local,format_string);
    }
    it = linked_list_iterator_next(it);
  }
  u->state = EXPLORED;
  return local;
}

void sweep(graph * gg, char * format_string) {
	graph_struct* g = (graph_struct*)gg;
  linked_list_iterator* it = linked_list_iterator_new(g->nodes);
  while(it){
    graph_node_struct* node = (graph_node_struct*)linked_list_iterator_getvalue(it);
    node->state = UNEXPLORED;
    it = linked_list_iterator_next(it);
  }
  
  int time = 0;
  
  linked_list_iterator* i = linked_list_iterator_new(g->nodes);
  while(i){
    graph_node_struct* node = (graph_node_struct*)linked_list_iterator_getvalue(i);
    time += sweep_aux(node,time,format_string);
    //printf("FATT\n");
    i = linked_list_iterator_next(i);
  }
}




