#ifndef TREEC
#define TREEC

#include "tree.h"
#include "node.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int nodeInTree(Tree *t, Node *v) {
    if(v == NULL) return 0;
    if(v == t->root) return 1;
    return nodeInTree(t, v->parent);
}

Tree *newTree() {
    Tree *new = malloc(sizeof(Tree));
    new->root = newNode("Volume", 1024, 1);
    new->size = 1;
    return new;
}

/* DA IMPLEMENTARE */
Node *insertFile(Tree *t, Node *p, char *nome, int size){
    Node* n = newNode(nome,size,0);
    n->parent = p;
    if(p->firstChild){
      Node* child = p->firstChild;
      p->firstChild = n;
      n->nextSibling = child;
    }
    t->size++;
    return n;
}

/* DA IMPLEMENTARE */
Node *insertDir(Tree *t, Node *p, char *nome){
    Node* n = newNode(nome,1024,1);
    n->parent = p;
    if(p->firstChild){
      Node* child = p->firstChild;
      p->firstChild = n;
      n->nextSibling = child;
    }
    else p->firstChild = n;
    t->size++;
    return n;
}

int ha_figli_directory(Node* node){
    if(!getIsDir(node)) return 1;
    //printf("SONO UN DIR %s %d\n",node->nome,getIsDir(node));
    int ha_directory = 0;
    Node* child = node->firstChild;
    while(child){
     // if(strcmp(node->nome,"dir6") == 0) printf("CHILD %s  IS DIR %d\n"
       //                                     ,child->nome,getIsDir(child));
      if(getIsDir(child)) ha_directory = 1;
      child = child->nextSibling;
    }
    return ha_directory;
}

void aggiungiChild(Node* node){
    Node* parent = node->parent;
    Node* c = parent->firstChild;
    while(c){
      c = c->nextSibling;
    }
    c = child;
}


int remove_node(Node*  node){
    Node* child = node->firstChild;
    int tot = 0;
    Node* temp;
   // printf("\nCANCELLO NODE %s\n",node->nome);
    while(child){
      temp = child;
      child = child->nextSibling;
      tot += temp->size;
      free(temp);
    }
    tot += node->size;
    if(node->nextSibling) aggiungiChild(node);
    //lse node->parent->firstChild = node->nextSibling;
    free(node);
    return tot;
}

int cleanSmallDirs_aux(Node* node, int min){
    if(!node) return 0;
    int del=0;
    printf("NODE %s\tHA FIGLI %d\tIS MIN %d\tCANCELLO %d\n",node->nome,
           ha_figli_directory(node), node->size < min,
              !ha_figli_directory(node) && node->size < min);
    if(!ha_figli_directory(node) && node->size < min) del = remove_node(node);
    
    return del + cleanSmallDirs_aux(node->firstChild,min) +
                          cleanSmallDirs_aux(node->nextSibling,min);
}


/*int cleanSmallDirs_aux(Node* node, int min){
    if(!node) return 0;
    int del=0;
    printf("NODE %s\tHA FIGLI %d\tIS MIN %d\tCANCELLO %d\n",node->nome,
           ha_figli_directory(node), node->size < min,
              !ha_figli_directory(node) && node->size < min);
    
    int ret = cleanSmallDirs_aux(node->firstChild,min) +
                          cleanSmallDirs_aux(node->nextSibling,min);
  if(!ha_figli_directory(node) && node->size < min) del = remove_node(node);
  return del + ret;
}*/

/* DA IMPLEMENTARE */
int cleanSmallDirs(Tree *t, int min) {
    return cleanSmallDirs_aux(t->root,min);
    //return 0;
}

/*void findAll_aux(Node* node, char* s, queue* q){
    if(!node) return;
    if(strcmp(node->nome,s) == 0){
      stampaQueue(q);
      !!!!!!!
    }
    enqueue(q,node);
    findAll_aux(node->firstChild,s,q);
    
}*/

/* DA IMPLEMENTARE */
void findAll(Tree *t, char *s) {
    /*queue* q = newQueue();
    Node* node = t->root;
    findAll_aux(t,s,q);*/
    return;
}


#endif
