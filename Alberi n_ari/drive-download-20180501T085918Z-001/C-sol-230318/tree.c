#include <stdlib.h>
#include <stdio.h>

#include "tree.h"

typedef struct node {
    int value;
    struct node * first_child;
    struct node * next_sibling;
} node;

node_tree * new_node_tree(int info) {
	node * n = malloc(sizeof(node));
	n->value = info;
	n->first_child = NULL;
	n->next_sibling = NULL;

	return (node_tree *) n;
}

int get_node_info(node_tree * n) {
	return ((node *) n)->value;
}

void set_first_child(node_tree * n, node_tree * first_child) {
	((node *) n)->first_child = first_child;
}

node_tree * get_first_child(node_tree * n) {
	return ((node *) n)->first_child;
}

void set_next_sibling(node_tree * n, node_tree * next_sibling) {
	((node *) n)->next_sibling = next_sibling;
}

node_tree * get_next_sibling(node_tree * n) {
	return ((node *) n)->next_sibling;
}

void delete_tree(node_tree * nt) {

	if (nt == NULL)
		return;

	node * n = (node *) nt;

	// printf("Deleting node: %d\n", n->value);
	
	delete_tree(n->first_child);
	delete_tree(n->next_sibling);
	free(n);
}

void print_tree_ric(node * n, int depth) {

	if (n == NULL)
		return;

	int i;
	for (i = 0; i < depth; i++)
		printf("-");

	printf("%d\n", n->value);
	print_tree_ric(n->first_child, depth + 1);
	print_tree_ric(n->next_sibling, depth);
} 

void print_tree(node_tree * node) {
	print_tree_ric(node, 0);
}
