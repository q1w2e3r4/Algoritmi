# aiml_ros

The `aiml_ros` implements a Dialogue Manager through AIML Knowledge Bases. This node depends on `PyAIML`[^2], an interpreter designed to correctly handle AIML files.

The node is subscribed to the `/best_speech_hypothesis` topic, containing the best transcription provided by the LU4R Android app. Whenever a new transcription is published onto the topic, a callback function processes the user’s utterance, gathering the next dialogue turn from the AIML KB. The result is finally published onto the `/dialogue_manager_response`.

## Dependencies

The package depends on the following resources:

 * `rospy` [http://wiki.ros.org/rospy]
 * `aiml` [`pip install aiml`]
 
## Communication

The node communicates with ROS through publishers/subscribers over the following topics.

### Subscribers

 * `/best_speech_hypothesis`: this topic contains the sentence uttered by the user.

### Publisher

 * `/dialogue_manager_response`: the ROS node publishes the response generated by the AIML KB onto this topic.

## Running

### Params

 * `aiml_path`: the path where AIML KBs are stored [can be either a file `.aiml` or a folder containing multiple `.aiml` files]

### Startup

```
$ cd [YOUR_PATH]/aiml_ros
$ roscore
$ rosrun aiml_ros aiml_ros_node.py _aiml_path:=KB/en
```

[^2]: Available at <https://pypi.python.org/pypi/PyAIML>
