#include <stdlib.h>
#include <stdio.h>
#include "tree.h"



node_tree * new_node_tree(int info) {
	node * n = malloc(sizeof(node));
	n->value = info;
	n->first_child = NULL;
	n->next_sibling = NULL;
	return (node_tree *) n;
}

int get_node_info(node_tree * n) {
	return ((node*)n)->value;
}

void set_first_child(node_tree * n, node_tree * first_child) {
	((node*)n)->first_child=first_child;
	return;
}

node_tree * get_first_child(node_tree * n) {
	return ((node*)n)->first_child;
}

void set_next_sibling(node_tree * n, node_tree * next_sibling) {
	((node*)n)->next_sibling=next_sibling;
	return;
}

node_tree * get_next_sibling(node_tree * n) {
	return ((node*)n)->next_sibling;
}

void delete_tree(node_tree * nt) {
	free(nt);
	return;
}

void print_tree_aux(node* n,int profondita){
	if(n==NULL) return;
	
	for(int i=0;i<profondita;i++){
		printf("-");
	}
	printf("%d\n",n->value);
	print_tree_aux(n->first_child,profondita+1);
	print_tree_aux(n->next_sibling,profondita);
	return;
}

void print_tree(node_tree * node) {
	print_tree_aux(node,0);
	return;
}
