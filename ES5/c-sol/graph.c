#include "graph.h"
#include <stdlib.h>
#include <stdio.h>

typedef enum {UNEXPLORED, EXPLORED, EXPLORING} STATUS;

typedef struct {
    
    void * value;
    linked_list * out_edges;

    // keep track status
    STATUS state;
    int timestamp;

} graph_node_struct;

typedef struct {
	GRAPH_TYPE type;
    linked_list * nodes;
} graph_struct;

graph * graph_new(GRAPH_TYPE type) {
	graph_struct * g = (graph_struct *) malloc(sizeof(graph_struct));
    g->nodes = linked_list_new();
    g->type = type;
    return g;
}

linked_list * graph_get_nodes(graph * gg) {
	graph_struct * g = (graph_struct *) gg;
	return g->nodes;
}

linked_list * graph_get_neighbors(graph * g, graph_node * n) {
	graph_node_struct * node = (graph_node_struct *) n;
    return node->out_edges;
}

graph_node * graph_add_node(graph * gg, void * value) {
 	graph_struct * g = (graph_struct *) gg;
	graph_node_struct * n = (graph_node_struct *) malloc(sizeof(graph_node_struct));
	n->value = value;
	n->out_edges = linked_list_new();
    linked_list_add(g->nodes, (void * ) n);
    return n;
}

void graph_add_edge(graph * gg, graph_node * from, graph_node * to) {
	
	graph_struct * g = (graph_struct *) gg;
	graph_node_struct * s = (graph_node_struct *) from;
	graph_node_struct * t = (graph_node_struct *) to;

    linked_list_add(s->out_edges, (void * ) t);
    if (g->type == UNDIRECTED)
    	linked_list_add(t->out_edges, (void * ) s);

	return;
}

void * graph_get_node_value(graph * g, graph_node * nn) {
	graph_node_struct * n = (graph_node_struct *) nn;
	return n->value;
}

void graph_delete(graph * gg) {
	
	graph_struct * g = (graph_struct *) gg;

	linked_list_iterator * i = linked_list_iterator_new(g->nodes);
	while (i != NULL) {

		graph_node_struct * n = linked_list_iterator_getvalue(i);
		linked_list_delete(n->out_edges);
		free(n);
		i = linked_list_iterator_next(i);

	}

    linked_list_delete(g->nodes);
    free(g);
}

static int get_path(graph_node * source, graph_node * target, linked_list * l) {

	graph_node_struct * s = (graph_node_struct *) source;
	graph_node_struct * t = (graph_node_struct *) target;

	if (s->state != UNEXPLORED) 
		return 0;

	s->state = EXPLORED;

	linked_list_iterator * lli = linked_list_iterator_new(s->out_edges);
    while (lli != NULL) {

        graph_node_struct * u = (graph_node_struct *) linked_list_iterator_getvalue(lli);
        lli = linked_list_iterator_next(lli);

        if (u->state == EXPLORED) {
        	continue;
        }

        linked_list_add(l, u->value);

        if (u == target)
        	return 1;

        else if (u->state == UNEXPLORED) {
        	int res = get_path(u, t, l);
        	if (res == 1)
        		return 1;
        	else
        		linked_list_remove_last(l);
        }
        
    }

    return 0;
}

linked_list * graph_get_path(graph * gg, graph_node * s, graph_node * t) {

	graph_struct * g = (graph_struct *) gg;

    linked_list_iterator * lli = linked_list_iterator_new(g->nodes);
    while (lli != NULL) {
    	graph_node_struct * n = (graph_node_struct *) linked_list_iterator_getvalue(lli);
        n->state = UNEXPLORED;
        //printf("setting %s as UNEXPLORED\n", (char *) n->value);
        lli = linked_list_iterator_next(lli);
    }

	linked_list * l = linked_list_new();
	linked_list_add(l,((graph_node_struct * )s)->value);
	
	int found = get_path(s, t, l);
	if (found == 1)
		return l;
	else {
		linked_list_delete(l);
		return NULL;
	}
}


static int sweep_aux(graph_node * source_node, int time, char * format_string) {

    graph_node_struct * source = (graph_node_struct *) source_node;
    if (source->state != UNEXPLORED) return 0;

    int loctime = 1;
    source->state = EXPLORING;
    source->timestamp = time;
    
    linked_list_iterator * lli = linked_list_iterator_new(source->out_edges);
    while (lli != NULL) {

        graph_node_struct * u = (graph_node_struct *) linked_list_iterator_getvalue(lli);
        
        printf("\t");
        printf(format_string, source->value);
        printf("(%d)->", source->timestamp);
        printf(format_string, u->value);
        printf("(%d) ", u->timestamp);

        if (u->state == EXPLORED) {
            
            if (source->timestamp < u->timestamp)
                printf("FORWARD\n");
            else
                printf("CROSS\n");

        } else if (u->state == EXPLORING) {
            printf("BACK\n");
        
        } else {
            printf("TREE\n");
            loctime += sweep_aux(u, time + loctime, format_string);
        }

        lli = linked_list_iterator_next(lli);
    }
    
    source->state = EXPLORED;
    return loctime;
}

void sweep(graph * g, char * format_string) {

    graph_struct * ref = (graph_struct *) g;

    linked_list_iterator * lli = linked_list_iterator_new(ref->nodes);
    while (lli != NULL) {
    	graph_node_struct * n = (graph_node_struct *) linked_list_iterator_getvalue(lli);
        n->state = UNEXPLORED;
        n->timestamp = 0;
        lli = linked_list_iterator_next(lli);
    }
    
    int loctime = 0;
    lli = linked_list_iterator_new(ref->nodes);
    while (lli != NULL) {

        graph_node_struct * node = (graph_node_struct *) linked_list_iterator_getvalue(lli);
        
        printf("Root ");
        printf(format_string, node->value);
        printf("\n");
        
        loctime += sweep_aux(node, loctime, format_string);
        lli = linked_list_iterator_next(lli);
    }
}
