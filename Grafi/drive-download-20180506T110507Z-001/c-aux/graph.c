#include "graph.h"
#include <stdlib.h>
#include <stdio.h>

typedef enum {UNEXPLORED, EXPLORED, EXPLORING} STATUS;

typedef struct {
    void * value;
    linked_list * out_edges;
    STATUS state;
    int timestamp;

} graph_node_struct;

typedef struct {
	GRAPH_TYPE type;
    linked_list * nodes;
} graph_struct;

graph * graph_new(GRAPH_TYPE type) {
	graph_struct* g=malloc(sizeof(graph_struct));
	g->type=type;
	g->nodes=linked_list_new();
    return g;
}

linked_list * graph_get_nodes(graph * gg) {
	graph_struct* g=(graph_struct*)gg;
	return g->nodes;
}

linked_list * graph_get_neighbors(graph * g, graph_node * n) {
	graph_node_struct* nn=(graph_node_struct*)n;
    return nn->out_edges;
}

graph_node * graph_add_node(graph * gg, void * value) {
	graph_node_struct* n=malloc(sizeof(graph_node_struct));
	n->value=value;
	n->out_edges=linked_list_new();
	n->state=UNEXPLORED;
	n->timestamp=0;
	graph_struct* g=(graph_struct*)gg;
	linked_list_add(g->nodes,(void*)n);
	return n;
}

void graph_add_edge(graph * gg, graph_node * from, graph_node * to) {
	graph_struct* g=(graph_struct*)gg;
	graph_node_struct* f=(graph_node_struct*)from;
	graph_node_struct* t=(graph_node_struct*)to;
	if(g->type==UNDIRECTED){
		linked_list_add(f->out_edges,(void*)t);
		linked_list_add(t->out_edges,(void*)f);
	}else{
		linked_list_add(f->out_edges,(void*)t);
	}
	return;
}

void * graph_get_node_value(graph * g, graph_node * nn) {
	graph_node_struct* n=(graph_node_struct*)nn;
	return n->value;
}

void graph_delete(graph * gg) {
	graph_struct * g = (graph_struct *) gg;
	linked_list_iterator * i = linked_list_iterator_new(g->nodes);
	while (i != NULL) {
		graph_node_struct * n = linked_list_iterator_getvalue(i);
		linked_list_delete(n->out_edges);
		free(n);
		i = linked_list_iterator_next(i);
	}
    linked_list_delete(g->nodes);
    free(g);
}

linked_list* DFS(linked_list* list, graph_node_struct* sorg, graph_node_struct* dest){
	if(sorg != dest){
		sorg->state=EXPLORED;
	    linked_list_add(list,sorg->value);
	}else{
		linked_list_add(list,sorg->value);
		return list;
	}
	for(linked_list_iterator* iter=linked_list_iterator_new(sorg->out_edges); iter!=NULL; iter=linked_list_iterator_next(iter)){
		graph_node_struct* value=(graph_node_struct*)linked_list_iterator_getvalue(iter);
		if(value->state==UNEXPLORED){
			list=DFS(list,value,dest);
			if(linked_list_get(list,linked_list_size(list)-1)==dest->value) return list;
			linked_list_remove_last(list);
		}
	}
	return list;
}


linked_list * graph_get_path(graph * gg, graph_node * s, graph_node * t) {
	//graph_struct* g=(graph_struct*)gg;
	graph_node_struct* sorg=(graph_node_struct*)s;
	graph_node_struct* dest=(graph_node_struct*)t;
	linked_list* list=linked_list_new();
	//tutti i nodi sono stati già marcati inesporati
	list=DFS(list,sorg,dest);
	return list;
}

int sweep_aux(graph_node_struct* node,int timestamp){
	node->timestamp=timestamp;
	for(linked_list_iterator* iter=linked_list_iterator_new(node->out_edges); iter!=NULL; iter=linked_list_iterator_next(iter)){
		graph_node_struct* value=(graph_node_struct*)linked_list_iterator_getvalue(iter);
		if(value->state==UNEXPLORED){
			printf("TREE EDGE: %s(%d) -> %s(%d)\n",node->value,node->timestamp,value->value,value->timestamp);
			value->state=EXPLORING;
			timestamp=sweep_aux(value,timestamp+1);
		}else if(value->state==EXPLORING){
			printf("BACK EDGE: %s(%d) -> %s(%d)\n",node->value,node->timestamp,value->value,value->timestamp);
		}else if(value->state==EXPLORED){
			if(value->timestamp < node->timestamp){
				printf("CROSS EDGE: %s(%d) -> %s(%d)\n",node->value,node->timestamp,value->value,value->timestamp);
			}else{
				printf("FORWARD EDGE: %s(%d) -> %s(%d)\n",node->value,node->timestamp,value->value,value->timestamp);
			}
		}
	}
	node->state=EXPLORED;
	return timestamp;
	
}


void sweep(graph * g, char * format_string) {
	graph_struct* gg=(graph_struct*)g;
	int timestamp=0;
	linked_list_iterator* iter=linked_list_iterator_new(gg->nodes);
	for(;iter!=NULL;iter=linked_list_iterator_next(iter)){
		graph_node_struct* value=(graph_node_struct*)linked_list_iterator_getvalue(iter);
		printf("Nodo Iniziale:%s\n",value->value);
		if(value->state==UNEXPLORED){
			timestamp=sweep_aux((graph_node_struct*)linked_list_iterator_getvalue(iter),timestamp);
			timestamp++;
		}
		printf("\n");
	}
	
	return;
}
