#include <stdlib.h>
#include <stdio.h>

#include "iter_postorder.h"
#include "stack.h"

void iterative_postorder_visit(node_tree * n) {
	node* nodo=(node*)n;
	stack* pila=new_stack();
	stack* perm=new_stack();
	stack_push(pila,nodo);
	while(stack_size(pila)!=0){
		nodo=stack_pop(pila);
		stack_push(perm,nodo);
		nodo=nodo->first_child;
		while(nodo){
			stack_push(pila,nodo);
			nodo=nodo->next_sibling;
		}
	}
	nodo=stack_pop(perm);
	while(nodo){
		printf("%d ",nodo->value);
		nodo=stack_pop(perm);
	}
}
